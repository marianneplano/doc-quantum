<?php

$header = [

	"title",
	"name",
	"year",
	"details",
	"links"
];


$body = [

	"key_points",
	"glossary",
	"theory",
	"about_artist",
	"about_cccb",
	"about_catalog",
	"about_website",
	"about_curator",
	"bio_catalog",
	"bio_website"
];