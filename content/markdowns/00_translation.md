# Translation

## EN > FR

* **bewilderment**: confusion, perplexité
* **linchpin**: pillier
* **beam**: rayon
* **aparatus**: appareil, équipement, instrument
* **merely**: simplement, seulement

## FR > EN

* **démarche**: process
* **intervalles réguliers**: regular intervals
* **détourner**: hijack


