---
title: Yunchul Kim
name: Cascade
year: 2018
details: |
    Three sculptural elements. **Argos**: Geiger–Müller tube, glass, aluminium, micro controller (480 x 400 mm); **Impulse**: non-pulsating pump, solenoid valve, micro controller, acrylic, aluminium (2300 x 2000 mm); **Tubular**: PDMS, micro tube (dimensions variable).
links:
    - https://yunchulkim.net/
    - https://yunchulkim.net/work/impulse-cascade-project/
glossary: |
    particle, elementary particle, muon particle
key_points: |
    - Contemplative.
    - Based on a muon detector.
    - Makes the invisible perceptible.
    - Self-made by the artist.
    - 3 parts
        * **Argos**, a muon detector with 41 different channels that capture the natural particles reaching the gallery space.
        * **Impulse**, a large-scale, complex fluid-transfer system;
        * **Tubular**, an 18-metre-long transparent tube;
about_catalog: |
    The study of the interactions of subatomic particles, as well as their fundamental constituents, is the basis of particle physics. An examination of these connections in experiments that reveal their most visible aspects is the first step to understanding them. In his work, Yunchul Kim explores the artistic potential of these amazing behaviors and reflects on the qualities of experimental materials that can reveal certain properties. For Kim, the materials with which he makes his works are not merely a formal support, they are actually objects for experimenting and observing the phenomena that take place at a fundamental level of the material. Cascade is based on an exploration of muons, an elementary particle in the lepton group that is generated in the cosmic rays crossing the atmosphere.

    The installation comprises three live elements: 
    * **Argos**, a muon detector with 41 different channels that capture the natural particles reaching the gallery space.
    * **Impulse**, a large-scale, complex fluid-transfer system;
    * **Tubular**, an 18-metre-long transparent tube; 

    When the particles are detected by the Argos system, a signal is sent to Impulse to trigger a response in the flow system in Tubular to keep the fluid flowing constantly through the sculpture. The artist aims to show the transition that takes place through the fluctuation of invisible events that form in the material. Cascade invites spectators to immerse themselves in the amorphous fluid landscapes and the apparently invisible world of fundamental matter.

    This work was developed as part of the Collide International Award, a partnership programme between Arts at CERN and FACT and was co-produced by ScANNER. With the support of the Korean Cultural Centre UK and Arts Council Korea. Yunchul Kim was artist in residency at CERN in 2017.

about_cccb: |
    * This is a contemplative artwork. 
    * The artist, Yunchul Kim, has great knowledge of different materials. He creates and designs all of the materials part of his artworks. 
    * This installation has three parts: 
        * Argos. This one’s purpose is to detect muons, a fundamental particle. Every time Argos detects this sort of particle, it sends a signal to Impulse via Wi-Fi.
        * Impulse. This part of the artwork receives the previous signal and then moves the pistons that compress the liquid inside the tubes, making the liquid flow.
        * Tubular. The third component is composed of wide tubes that allow the circulation of the liquid (water and glycerine). Eventually, the liquid gets back to Impulse. 
    * As said before, we do not directly perceive the microscopical world (governed by quantum laws), so some of the artist in the exhibition have taken some abstract theories on what happens in the quantum regim and have transformed them into some sort of formal representation that we can perceive with our senses.
    * Then, what is intentionality behind this piece? Yunchul Kim was inspired by muons, that elementary particles that have a role in the quantum world but obviously we cannot perceive them. So he tries to emerge this world by making this liquid flow through the tubes.
about_curator: |
    This is a rather complex installation made of 3 parts. The artist, while he was at CERN, he was interested in a particular particle that is called a *muon*. A muon is a relatively large particle, within the diverse type of particles that constitute matter. This part of the work which is called *Argos*, the work starts with an assembly of 3 parts, so this detector, this is a Geiger detector, that detects the muons that are in the environment, which is, 2 or 3 muons every minute perhaps, and the muons which are detected go to these very complex ? to be translated into electric inputs, and those inputs are sent to this other part of installation, this amazing chandelier, which is called *Impulse*, so those electrical signs are sent in this hydraulic system and this will trigger the different pistons here, and from there on you will get to these micro tube, in this kind of liquid that is a way is micro-tubes mimicking something that we don't see, which are the muons. You will here, in the next phase, in these micro-tubes called *Tubular*, you will see little bubbles, these are not the muons but in a way it's writing through the eyes of the artist, what we cannot see. So it's also, I think it's a rather poetic piece, but it's also technologically, as a work rather complex and in a way shows the complexity of the artefacts that we need in order to uncover what we cannot see. Once more, this is a poetic license, a metaphor that shows us also that complexity, that in an evidence in the experiment run at CERN.
bio_catalog: |
    Yunchul Kim is an artist, an electroacoustic music composer, and the founder of Studio Locus Solus in Seoul. His latest works focus on the artistic potential of fluid dynamics, metamaterials, and especially on magnetohydrodynamics.
bio_website: |
    Yunchul Kim is an artist, an electroacoustic music composer, and the founder of Studio Locus Solus in Seoul. His latest works are focusing on the artistic potential of fluid dynamics, metamaterials (photonic crystals) and especially on the context of magnetohydrodynamics. His works have been shown internationally including: FACT, Liverpool; ZKM, Germany; Ars Electronica, Austria; International Triennial of New media art, China; VIDA15.0, Spain; Transmediale, Germany; ISEA, Germany; and New York Digital Salon, amongst others.

    Kim was the winner of the Collide International Award 2016, CERN, and was awarded the third prize at VIDA 15.0, Vida Foundation in 2013. He has received grants from renowned institutions and organizations such as Ernst Schering Foundation, Edith-Russ-Haus for Media Art.

    Having taught in several academic institutions, Kim was chief researcher of the research group Mattereality at the Transdisciplinary Research Program at the Korea Institute for Advanced Study. He is a member of the art and science project group Fluid Skies as well as Liquid Things, an artistic research project at the Art and Science Department of the University of Applied Arts Vienna, Austria.
---