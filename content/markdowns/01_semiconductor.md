---
title: Semiconductor
name: The View from Nowhere
year: 2018
details: |
  Single-channel digital video with sound
  (13:11 mins)

links:
  - https://semiconductorfilms.com/art/
  - https://en.wikipedia.org/wiki/Semiconductor_(artists)
  - http://www.simonings.net/?p=2051
  - https://semiconductorfilms.com/art/the-view-from-nowhere-2/

key_points: |
  - Objective observation > « ethnography study » on lab culture
    - *The View from Nowhere* == position scientists should adopt to stay objective > here artists intend to so as well
  - Two sides of physics depicted:
    - Voice > theoretical physicists (Luis Álvarez-Gaumé, John Ellis)
    - Images > experimental physics' machines
  - Animations = particle collisions

theory: |
  Two kinds of particle physicists:
  * Experimental physicists: Big machines, run the experiments, analyze the data, and try to discover things predicted by theory > TEST
  * Theorists: Construct the theories that try to explain everything we see in nature > GUIDELINES

about_catalog: |
  Particle physics deals with the study of matter and requires incredibly advanced technology, as evidenced by the Large Hadron Collider (LHC) at CERN: a particle accelerator built by the scientists and engineers in the laboratory to better understand the fundamental components and nature of our universe. *In The View from Nowhere*, Semiconductor offer an objective observation of the practices, techniques, artefacts and languages developed by scientists at CERN, revealing in their images the rich culture of the laboratory. The voices of theoretical scientists Luis Álvarez-Gaumé and John Ellis recount the task of the scientist and the many issues that inform their scientific viewpoints and intuitions: how do they conceive of and describe reality? What is their viewpoint as observers? To what extent is science today our way of connecting with nature and its mysteries? *The View from Nowhere* focuses on the voice of both scientists, as well as the artefacts built to test their ideas according to their predictions and theories. The artists are largely inspired by laboratory archives, films made during their residency at CERN, and computer-generated animations.


about_website: |
  *The View from Nowhere* is a single-channel moving image work which explores man’s place in nature through the science and technology of CERN, the particle physics laboratory in Geneva.

  Driven by an interest in the material nature of our physical world and how we experience it through the lens of science and technology, Semiconductor go looking for the techniques that are developed at CERN which ask fundamental questions about nature, and the languages which ensue to make sense of it.

  Through juxtaposing discussions around the application and processes of theoretical physics with filmed footage in CERN’s hi-tech workshops, Semiconductor explore the dichotomy that is revealed between the surprisingly creative pursuit of theoretically modeling our physical universe and the fixed/hard classical nature of producing instrumentation to test these notions. It reveals a sense of the scientific frameworks developed by man to explore matter beyond the limits of human experience, whilst raising questions about our place in the larger nature of reality.

  The title *The View from Nowhere* refers to the philosophical concept that science should remain an objective analysis of the natural world, if it is to be seen as having value.

about_cccb: |
  * When they stayed at Cern they got inspired by both experimental and theoretical physicists. 
  * The work is an objective observation of the practices, techniques and languages developed by scientists at Cern. It’s an ethnography study on the lab culture. 
  * The images we see in the video are related to the tasks of experimental physicists (dealing with machines and ‘experimenting’ constantly) whereas the monologues we hear come from the voices of theoretical physicists, whose work consists of going to the office, study and think of some theories or formula that can explain the world in a better way. 
  * Somehow, this piece shows us the real work of physicists, which often is idealized. It’s not as if they came to work one day and spontaneously discovered something great. They may work on the same formula for decades in order to prove something. 
  * There are some objects in the shelf. They are not part of the work of art, but the curators decided to bring this objects in to complement the work. These objects might seem contemporary sculptures but they are actually lab objects used at CERN. 
  * To sum up: this work shows us the world of the scientist and his work. The approach of the artists is the same as the scientists when they do research.

about_curator: |
  We always try to place this video work in the introduction, it brings you to the interior of the lab. This is CERN, we have 300 buildings, thousands of experiments. The artists spent weeks inside this places looking at ways people build the technology, gather in seminars, working with the matter, with magnets, heavy technical engineering. If you hear the audio, you will hear two people talking. They are Álvarez-Gaumé, theoretical physicist who's now in NY, and Joe Ellis, imperial college. What you will hear is them talking about how science is done at different levels, at personal level, what is the passion behind how ideas are created, idea that are as abstract as we can imagine. These 2 people are explaining to the artist the way they construct their thinking. The work is called *The View from Nowhere* and *The View from Nowhere* is a position ethnographs take not to affect their environment which is just the opposite of the idea of physics, that research everything about interactions. It combines also animation, these animations you are seeing are particle collisions, out of data from the experiment.

bio_catalog: |
  Semiconductor is artist duo Ruth Jarman and Joe Gerhardt. Their work is based on an exploration of the philosophical foundations of science and the current state of these discussions to manifest the construction of ideas, stories and institutional contexts that produce scientific knowledge.

bio_website: |
   Semiconductor is UK artist duo Ruth Jarman and Joe Gerhardt. Over twenty years they have become known for an innovative body of work, which explores the material nature of our world and how we experience it through the lenses of science and technology. They occupy a unique position in the art world, blending, in philosophically compelling ways, experimental moving image techniques, scientific research and digital technologies. Man’s experience of the physical world is central to the work of Semiconductor; taking us beyond the everyday rigid, static matter bound by the limits of human perception. By extending vision, hearing, time and scale through the use of technologies, by transcending physical constraints, Semiconductor creates first person experiences. These disrupt our everyday assumptions about reality and encourage us to step outside our fixed vantage points in space and time, to experience places that are in a constant state of flux. Through a contemporary reworking of the sublime the work is at once both humbling and captivating. Semiconductor’s works often evolve from intensive periods of research spent in science laboratories, including CERN, Geneva (2015); NASA Space Sciences Laboratory, UC Berkeley, California (2005); Mineral Sciences Laboratory, Smithsonian National Museum of Natural History (2010); and the Charles Darwin Research Station, Galapagos (2010). By examining the techniques and processes that each discipline employs to understand the physical matter of the world, Semiconductor reveal the flaws and ruptures within scientific data, exposing the human signature which is ascribed within scientific findings and emphasizes the presence of the observer. Through making visible the tools and materials used by scientists Semiconductor observes the work of the observers, drawing attention to the ways in which science mediates our experiences of nature.
---
