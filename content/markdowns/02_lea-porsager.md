---
title: Lea Porsager
name: CØSMIC STRIKE
year: 2018
details: |
  *Tired Tantric Experiment* (single-channel 3D film with sound (62:00)); 
  *Daybeds* (foam-mattresses, bedspreads (dimensions variable)); 
  anaglyph 3D glasses; 
  *Sushumna Nadi Avatar* (original neutrino horn from CERN)
links:
  - https://www.leaporsager.dk/
  - https://nilsstaerk.dk/artists/lea-porsager
key_points: |
  - Mediation, like science, as a way of acquiring knowledge & seeing the invisible.
  - Process: 
    - a meditation in a CERN office
    - the next day, the artist found a neutrino horn found in CERN storage space
  - Result:
    - The video is inspired by the visions from the meditation & is accompanied by a mantra.
    - The neutrino horn is seen as a "ghostly container of oscillation, vibrations".
    - 3D glasses : reference to third eye from spiritual traditions in India
      - that shows beyond the physical world 
      - & can be made more powerful through meditation
glossary: |
  particle, elementary particle, neutrino, neutrino horn, third eye
about_catalog: |
  The work of Lea Porsager seeks to associate spirituality, sensuality and language, offering alternative means by which to experience and situate science today. An immersive 3D animation generates visions from a *Tired Tantric Experiment*, a 62 minute long Kundalini meditation undertaken by Porsager in one of CERN’s empty offices. The visions are accompanied by the sound of a mantra (“Isht Sodhana Mantra Kriya”) and a neutrino horn (here titled: *Sushumna Nadi Avatar*) from CERN’s storage facility.
  *CØSMIC STRIKE* explores the neutrino: an enigmatic, mysterious particle that challenges some of the models currently being researched in contemporary science. Among the most abundant particles in the universe, they only interact via weak subatomic force and gravity. The displayed horn is a high-current, pulsed focusing device, that selects the particles and focuses them into a sharp beam.
  Through the animation, the audience is invited to experience ‘neutrino-imaginations’ - a strange mirage from the inside of the horn, which Porsager perceives as a ghostly container of oscillation, vibrations and irritation, which manifests a collision of different technologies. *CØSMIC STRIKE* is a superposition of hard science and loopy mysticism which aims to invoke a repetitive, occult, and oddly interstellar scene. By disrupting quantum technologies with esoteric propositions, Porsager’s work engages with a myriad of impossible and impassable worlds, with a suggestive call for other perceptions.
about_cccb: |
  - So this is one of the most peculiar works of the exhibition. 
  - When Lea Porsager arrived at CERN, she asked if she could wander around the place freely in order to explore and find something that could catch her attention. She found two things: 
      - The neutrino horn from CERN. A neutrino is a particle that barely has mass, so it is really hard to study, although we are constantly crossed by neutrinos. What this horn does, is hold a neutrino for a second so they could study it. 
      - An empty office in CERN. 
  - The procedure. She locked herself in that office for 62 minutes to perform a tired tantric experiment (which included yoga, meditation, etc.). From what she experienced there doing that, she created the videos you have in these screens. What you see in them represents an on-going journey through the insides of the neutrino horn. The whole experience is supposed to be a connection with a neutrino. 
  - The aim of this work is that you sit here and watch the video while wearing these glasses she made and listening to a mantra. This way, you’re supposed to get in a state that will allow you to feel the same thing she experienced at CERN. 
  - What can we take from all of this?
      - Lea Porsager combines technology and conscience. She makes a superposition of esotericism and science. 
      - Since quantum physics revolutionizes how we understand our world and nature, Lea Porsager wonders if there can be other ways of approaching knowledge. We can also learn from introspection and through esotericism. She’s searching for that mystical moment of connection with One, with Divinity, in order to get full knowledge about our nature. Not that far from what mystics did back in the day or how this notion or concept has been explored throughout the arts: The ecstasy of Saint Teresa by Bernini. [We can also mention CCCB’s exhibition Black Light (summer of 2018)]. 
about_website: |
  *Ø enjøyment, Ø jøy. Ø y.*
  *Ø ø ø ø ø ø ø ø ø øngoing gangbang.*
  *Ø unhøly impact øf cølliding cløuds! Ø! STRIKE!*
  *Yet anøther ghøstly superpøsition øf hard science and løøpy mysticism.*
  *Dharti Hai Akash Hai! The vastness øf the ether cløud! The Earth is. The Ethers are.*
  *Ø jøy! Zigzig and perpendicular! Zilliøns øf thøughts cøllide, galvanized, cutting acrøss zpace-time!*

  On a bedspread, a Tired Tantric Experiment was executed at CERN. Two tulpas (avatars) were initiated: artist Hilma af Klint (1862–1944) and artist Lee Lozano (1930–1999). Af Klint’s primordial chaos rubbed up against Lozano’s cool tool. Set in a cloudy atmosphere. Ø strike again!
  Af Klint appears as (en)lightning energy going intracloud (IC), cloud to cloud (CC), and at certain points cloud to ground (CG) and ground to cloud (GC). A collision between af Klint’s luminous
  atoms and Lozano’s terrestrial, turbulent corpus. A weird, low-key dropout experiment of self-touching, in which Lozano and af Klint’s clash produces a tiny amorph figure, born again and again.
  Endless iterations. A series of lively jolts. The hot plasma of cosmic rays flow down through theserpentine pathways of the nervous system as Kundalini rising.
  A solitary screen displays the 62-minute 3D animation (Tired Tantric Experiment), accompanied bythe sound of a 62-minute mantra (Isht Sodhana Mantra Kriya), a solitary bedspread (Plasma Sheet),and a solitary conical object (Sushumna Nadi Avatar), pulled out of the hermetic grip of the EuropeanOrganization for Nuclear Research storage facility. No ownership, only locked, loaded and good toGo.
  Far-øut.
about_curator: |
  Cosmic Strike is a conbination of explorations of sensuality, language, matter, spiritualism and esoterism. Lea came to CERN and wanted to engage into the lab experience in a very singular way. She did her meditation practice during one day, and the second day, we went into hunting objects. We were looking for objects that she wanted to re-engage with. We found a neutrino horn in a storage place. It's technically a vaccum pipe, where vaccum inside, allows the interaction with the particle, which is the neutrino. It has zero interaction with any matter, which is why there is some mysteries here. So, in the storage places at CERN, many many objects that are almost sculptures. Lea wanted to bring it to the exihbition, and then she got back to her studio, she wroked with an artist on the videos that you see on the screen. In the screen, there is a representation of her spiritual experience of the lab, and she's trying to reach the neutrino, and understanding neutrino through the horn. How and what interction is, what reactions and impact had in her mind and in her body. She invites the audiance to lie down and put the glasses with a third eye, and this is actually something that any physicist would understand, why there is a third eye, because we are behind any supra perception level and she designed these glasses, so we can wear them and watch the films into the trip, looking for the neutrino.
bio_catalog: |
  Lea Porsager’s (DK) practice interweaves fabulation and speculation with a variety of mediums including film, sculpture, photography, and text. Her works encompass science, politics, feminism, and esotericism.
bio_website: |
  Lea Porsager graduated from the Royal Danish Academy of Fine Arts, Copenhagen, and theStädelschule, Frankfurt am Main, in 2010. She began her studies as a PhD candidate at Malmö ArtAcademy and Lund University in September 2015. Porsager’s practice interweaves fabulation andspeculation with a variety of mediums, including film, sculpture, photography, and text. Her worksencompass science, politics, feminism, and esotericism.Porsager’s recent solo exhibitions include Museum of Contemporary Art, Roskilde 2019; Nils Stærk,Copenhagen, 2016; Brandts, Odense, 2016; Göttingen Kunstverein, 2015; Overgaden, Copenhagen,2015; Künstlerhaus Bethanien, Berlin, 2015; Henie Onstad Kunstsenter, Høvikodden, 2013; andEmily Harvey Foundation, New York, 2013. Group exhibitions include National Taiwan Museum ofFine Arts, Taiwan, 2020; CCCB, Barcelona, 2019; iMAL, Bruxelles, 2019; SMK – National Galleryof Denmark, Copenhagen, 2017; Rebuild Foundation, Chicago, 2016; Den Frie, Copenhagen, 2016,2014; Monash University Museum of Art | MUMA, Melbourne, 2015; Moderna Museet, Malmö,2014; KUMU Art Museum, Tallinn, 2014; Sorø Kunstmuseum, 2014; Contemporary Art Centre,Vilnius, 2013; and Neue Gesellschaft für Bildende Kunst, Berlin, 2013.Porsager was selected as a CERN Honorary Mention for the Collide International Award in 2018. In2012, Porsager participated in dOCUMENTA (13) with Anatta Experiment. She was awarded the CarlNielsen and Anne Marie Carl-Nielsen Scholarship in 2014. In 2015, Porsager partook in the 14thIstanbul Biennial: SALTWATER: A Theory of Thought Forms as Annie Besant’s “medium,” recreatingthirty-six of Besant’s watercolours from the book Thought-Forms: A Record of ClairvoyantInvestigation (1905). Porsager’s earthwork and memorial Gravitational Ripples was inaugurated inJune 2018 in Stockholm, Sweden, commemorating the Swedish lives lost in the 2004 tsunami inSoutheast Asia. She is represented by Nils Stærk Gallery, Copenhagen. In November 2020, Porsageropens a solo exhibition at Moderna Museet in Stockholm.
---








