# CERN

## What is CERN?

* Site: https://home.cern/about/who-we-are/our-mission
* Wiki: https://en.wikipedia.org/wiki/CERN

CERN > European Organization for Nuclear Research (Conseil européen pour la recherche nucléaire)

"At CERN, our work helps to uncover what the universe is made of and how it works. We do this by providing a unique range of particle accelerator facilities to researchers, to advance the boundaries of human knowledge.

The Laboratory, established in 1954, has become a prime example of international collaboration.

Our mission is to:

* provide a unique range of particle accelerator facilities that enable research at the forefront of human knowledge.
* perform world-class research in fundamental physics.
* unite people from all over the world to push the frontiers of science and technology, for the benefit of all."

## LHC > Large Hadron Collider

Biggest particle accelerator ever built
27km ring, speed up

 *Particle Fever*
"Recreate the conditions of the Big Bang"
"Understand something about the basic laws of physics" David Kaplan,

"Smash together tiny protons, wich are particlile inside evry atom. Run the proton around the ring multiple times to build up speed, almost at the speed of light."
"We have to smash particle together at high enough speed to disturb the field and create a Higgs particle"
"Beams of protons that collide"
"Scientists at CERN are trying to find out what the smallest building blocks of matter are."

*CERN Website*
The Large Hadron Collider (LHC) is the world’s largest and most powerful particle accelerator. It first started up on 10 September 2008, and remains the latest addition to CERN’s accelerator complex. The LHC consists of a 27-kilometre ring of superconducting magnets with a number of accelerating structures to boost the energy of the particles along the way.

Inside the accelerator, two high-energy particle beams travel at close to the speed of light before they are made to collide. The beams travel in opposite directions in separate beam pipes – two tubes kept at ultrahigh vacuum. They are guided around the accelerator ring by a strong magnetic field maintained by superconducting electromagnets. The electromagnets are built from coils of special electric cable that operates in a superconducting state, efficiently conducting electricity without resistance or loss of energy. This requires chilling the magnets to ‑271.3°C – a temperature colder than outer space. For this reason, much of the accelerator is connected to a distribution system of liquid helium, which cools the magnets, as well as to other supply services.

Thousands of magnets of different varieties and sizes are used to direct the beams around the accelerator. These include 1232 dipole magnets 15 metres in length which bend the beams, and 392 quadrupole magnets, each 5–7 metres long, which focus the beams. Just prior to collision, another type of magnet is used to "squeeze" the particles closer together to increase the chances of collisions. The particles are so tiny that the task of making them collide is akin to firing two needles 10 kilometres apart with such precision that they meet halfway.

All the controls for the accelerator, its services and technical infrastructure are housed under one roof at the CERN Control Centre. From here, the beams inside the LHC are made to collide at four locations around the accelerator ring, corresponding to the positions of four particle detectors – ATLAS, CMS, ALICE and LHCb.

