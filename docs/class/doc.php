<?php

class Doc extends Parts{

	public $work_fields;
	public $header;
	public $body;

	public function __construct($header, $body){

		$this->header = $header;
		$this->body = $body;
		$this->work_fields = array_merge($header, $body);
	}

	public function texts(){

		return $this->body;
	}


	public function Parts(){

		$parts = [];

		foreach($this->workFiles() as $file){

			$part = new stdClass;

			$part->nameId = $this->partName($file); 
			$part->num = $this->partNum($file);
			$part->id = $this->partId($file);

			if($this->isWork($file)){

				$part->type = "work";
				$datas = $this->loadYAML($file);

				foreach($this->work_fields as $field){

					if(isset($datas[$field])){

						$part->$field = $datas[$field];

					}else{

						$part->$field = "No ".$this->titlize($field);
					}

				}

			}else{

				$part->type = "intro";
				$part->content = file_get_contents($file);
			}

			$parts [$this->partId($file)] = $part;
		}

		return $parts;
	}

	public function part($get){

		if($this->isValid($get)){

			return $this->parts()[$get['part']];
		}
	}

}