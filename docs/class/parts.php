<?php

class Parts extends Strings{

	protected $md_dir = "content/markdowns";

	protected function workFiles(){

		return glob($this->md_dir."/*.md");

	}

	protected function partId($file){

		$md = substr($file, strpos($file, "_")+1);

		return substr($md, 0, strpos($md, "."));
	}

	public function partName($file){

		$id = $this->partId($file);

		return $this->titlize($id);

	}


	protected function partNum($file){

		$start = substr($file, 0, strpos($file, "_"));
		$num = intval(substr($start, strrpos($start, "/")+1));
		
		return $num;
	}


	protected function isWork($file){

		return ($this->partNum($file) > 0);

	}

	protected function partList(){

		$list = [];

		foreach($this->workFiles() as $file){

			$list [] = $this->partId($file);
		}

		return $list;
	}


	public function isValid($get){

		if(isset($get['part'])){

			$part = strval($get['part']);

			if(in_array($part, $this->partList())){

				return true;

			}else{

				$this->dump404();
			}

		}else{

			$this->dump404();

		}

	}

	public function isDefined($part, $field){

		return ($part->$field !== 'No '.$this->titlize($field));
	}

}