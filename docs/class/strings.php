<?php

class Strings extends Root{

	public function loadYAML($path){

		return spyc_load_file($path);
	}

	public function dumpYAML($data){

		return spyc_dump($data);
	}

	public function dumpMD($text){

		$parse = new Parsedown();

		return $parse->text(str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n"),"\n",$text));
	}

	public function slug($str) {

		$string = str_replace(' ', '-', $str); 
		
		return strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $str)); 
	}

	public function title($glyph, $str){

		if(strpos($str, $glyph)){

			$words = explode($glyph, $str);
			$title = [];

			foreach($words as $word){

				$title [] = ucfirst($word);
			}

			return implode(" ", $title);

		}else{

			return $str;
		}

	}

	public function titlize($str){

		$glyphs = ["-", "_"];

		foreach($glyphs as $glyph){

			$str = $this->title($glyph, $str);
		}

		return ucfirst($str);
	}

	public function showUrl($url){

		$begins = ["http://www.", "https://www.", "http://", "https://"];
		$back = '';

		foreach($begins as $begin){

			$sample = substr($url, 0, strlen($begin));

			if($sample == $begin && !strlen($back) > 0){

				$back = (substr($url, strlen($url)-1, strlen($url)-2) == "/") ? substr($url, strlen($begin), strlen($url)-strlen($begin)-1) :  substr($url, strlen($begin));

			}
		}

		return (strlen($back) > 0) ? $back : $url;
		
	}

}