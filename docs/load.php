<?php

/* LIBRARIES */

include "vendor/autoload.php";
include "vendor/Spyc.php";

/* GLOBAL CLASS */

include 'docs/class/root.php';
include 'docs/class/strings.php';
include 'docs/class/parts.php';
include 'docs/class/doc.php';

/* CONFIG */

include 'config/config.php';

$str = new Strings;
$doc = new Doc($header, $body);