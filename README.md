# Docs Quantum

A small doc website.

## Content

Generated from the markdown files in content/markdowns.

### Intro Files

The *intro files* start with 00_ and are written in plain markdown (plain markdown being displayed in web page).

### Work Files

The *work files* start with a two digits number (01_, 02_...) that matches the work they document. They’re written in yaml, following the fields that are specified in the config file.

## Config

config/config.php

- The fields to be used in works .md files.

 **Header** > To be displayed in page header.
 **Body** > To be displayed in page body.