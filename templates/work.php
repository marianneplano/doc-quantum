<?php include "snippets/header.php" ?>
<header>
	<div class="head">
		<div>
			<h1 class="<?= (!$doc->isDefined($part, 'title')) ? 'false' : '' ?>" ><?= $part->title ?></h1>
			<h2 class="<?= (!$doc->isDefined($part, 'name')) ? 'false' : '' ?>"><?= $part->name ?></h2>
			<div class="date <?= (!$doc->isDefined($part, 'year')) ? 'false' : '' ?>"><?= $part->year ?></div>
		</div>
		<div class="number"><?= $part->num ?></div>
	</div>
	<div class="details <?= (!$doc->isDefined($part, 'details')) ? 'false' : '' ?>">
		<?= $str->dumpMD($part->details) ?>
	</div>
	<div class="links <?= (!$doc->isDefined($part, 'link')) ? 'false' : '' ?>">
		<?php if($doc->isDefined($part, 'links')): ?>
			<ul>
				<?php foreach($part->links as $link): ?>
					<li><a href="<?= $link ?>" target="_blank"><?= $str->showUrl($link) ?></a></li>
				<?php endforeach ?>	
			</ul>
		<?php endif ?>	
	</div>
</header>
<main>
	<?php foreach($doc->texts() as $text): ?>
		<div class="text <?= (!$doc->isDefined($part, $text)) ? 'false' : '' ?>" id="<?= $text ?>">
			<?php if($doc->isDefined($part, $text)): ?>
				<h3><?= $str->titlize($text) ?></h3>
			<?php endif ?>	
			<?= $str->dumpMD($part->$text) ?>
		</div>
	<?php endforeach ?>	
</main>
<?php include "snippets/footer.php" ?>
