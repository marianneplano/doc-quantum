<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Quantum — <?= (isset($part) ? $part->nameId : 'Docs') ?></title>
	<link rel="stylesheet" type="text/css" href="assets/css/main.css">
</head>
<body class="<?= (isset($part)) ? $part->type : 'home' ?>" id="<?= (isset($part)) ? $part->id : 'home' ?>">
	<?php include "snippets/nav.php" ?>
