<nav <?= (!isset($part)) ? 'class="active"' : '' ?>>
	<div id="menu"></div>
	<div class="pages">
		<a href="index" <?= (!isset($part)) ? 'class="current"' : '' ?>>Home</a>
		<?php foreach ($doc->parts() as $id => $sub): ?>
			<a href="<?= $id ?>" <?= (isset($part) && $sub == $part) ? 'class="current"' : '' ?>><?= ($sub->num > 0) ? $sub->num.' ' : '' ?><?= $sub->nameId ?></a>
		<?php endforeach ?>
	</div>
		<?php if(isset($part) && $part->type == "work"){ ?>
		<div class="submenu">
			<?php foreach($doc->texts() as $text): ?>
				<?php if($doc->isDefined($part, $text)): ?>
					<a href="#<?= $text ?>"><?= $str->titlize($text) ?></a>
				<?php endif ?>	
			<?php endforeach ?>	
		</div>
	<?php } ?>
</nav>